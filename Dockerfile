FROM nullfox/nodejs-zmq

# Set the working dir
WORKDIR /root

# Copy in package json and run npm install
# Keep it up above our ADDs to optimize Docker layer caching
ADD package.json /root/package.json
RUN npm install

ADD index.js /root/index.js
ADD src /root/src

# Insert ADD, RUN, etc etc here

CMD ["npm", "start"]