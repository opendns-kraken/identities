var worker = require('kraken-core').Worker.instance();
var pkg = require('./package.json');
var Models = require('./src/models');

console.log('Starting %s backend', pkg.name);

Object.keys(Models).forEach((key) => {
  console.log('Registering model %s with Kraken', key);

  worker.registerCollection(new Models[key]);
});