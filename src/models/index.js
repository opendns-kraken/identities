'use strict';

var fs = require('fs');
var path = require('path');
var _ = require('lodash');

module.exports = _.chain(fs.readdirSync(__dirname))
  .without('index.js')
  .filter((file) => {
    return fs.statSync(path.join(__dirname, file)).isFile()
  })
  .map((file) => {
    var model = require(path.join(__dirname, file));

    return [model.name, model];
  })
  .fromPairs()
  .value();