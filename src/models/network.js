'use strict';

var _ = require('lodash');
var Bluebird = require('bluebird');
var rows = require('./data/network.json');

class Network {
  resources() {
    return [
      '/networks/{id:integer}'
    ];
  }

  responseSchema() {
    return {
      id: {
        type: 'integer',
        description: 'The primary key of the record',
        example: 125163,
        required: true
      },
      
      name: {
        type: 'string',
        description: 'The name of the network',
        example: 'Office Network',
        required: true
      }
    };
  }

  createUpdateSchema() {
    return _.pick(this.responseSchema(), ['name']);
  }

  list() {
    return Bluebird.resolve(rows);
  }

  create(params) {
    rows.push(params);

    return Bluebird.resolve(rows);
  }

  read(params) {
    return Bluebird.resolve(_.find(rows, { id: params.id }));
  }

  update(params) {
    this.read(params)
    .then((row) => {
      var idx = _.indexOf(rows, row);

      rows[idx] = _.merge(rows, params);

      return rows[idx];
    });
  }

  delete() {
    this.read(params)
    .tap((row) => {
      var idx = _.indexOf(rows, row);

      delete rows[idx];
    });
  }
};

module.exports = Network;